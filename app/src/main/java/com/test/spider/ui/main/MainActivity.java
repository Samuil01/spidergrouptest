package com.test.spider.ui.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.test.spider.R;
import com.test.spider.ui.foodlist.FoodListFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.root, FoodListFragment.newInstance())
                    .commit();
        }
    }
}
