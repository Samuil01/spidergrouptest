package com.test.spider.ui.foodlist;

import com.test.spider.data.model.Food;
import com.test.spider.data.network.INetModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * Created by Admin on 17.10.2017.
 */

public class FoodListPresenter {

    private INetModel netModel;
    private IFoodListFragment view;
    private Disposable disposable;
    private List<Food> foodList = new ArrayList<>();

    // Нужно ли обновить данные
    // если, например, при повороте экрана загрузка была прервана
    private boolean shouldUpdate;

    public FoodListPresenter(INetModel netModel) {
        this.netModel = netModel;
    }

    public void setView(IFoodListFragment view) {
        this.view = view;
    }

    public void setShouldUpdate(boolean shouldUpdate) {
        this.shouldUpdate = shouldUpdate;
    }

    public void getData() {
        if (shouldUpdate || disposable == null) {
            view.showLoader();
            disposable = netModel
                    .getData()
                    /*.startWith(foodList)*/
                    // Сохраняем список в поле класса
                    .doOnNext(dto -> foodList = dto)
                    .subscribe(
                            dto -> view.onSuccess(dto),
                            throwable -> view.onError(throwable.getMessage()),
                            () -> {
                                shouldUpdate = false;
                                view.onComplete();
                            }
                    );
        }
    }

    public List<Food> getStoreList() {
        return foodList;
    }

    public void onStop() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
            shouldUpdate = true;
        }
    }
}
