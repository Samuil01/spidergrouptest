package com.test.spider.ui.foodlist;

import com.test.spider.data.model.Food;

import java.util.List;

/**
 * Created by Admin on 17.10.2017.
 */

public interface IFoodListFragment {
    void onSuccess(List<Food> listFood);

    void onError(String message);

    void onComplete();

    void showLoader();
}
