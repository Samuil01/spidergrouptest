package com.test.spider.ui.foodlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.test.spider.App;
import com.test.spider.R;
import com.test.spider.data.model.Food;
import com.test.spider.ui.other.SpacesItemDecoration;

import java.util.List;

import javax.inject.Inject;

public class FoodListFragment extends Fragment implements IFoodListFragment {

    @Inject
    FoodListPresenter presenter;

    private RecyclerView recyclerView;
    private FoodAdapter foodAdapter = new FoodAdapter();
    private SwipeRefreshLayout swipeRefreshLayout;

    public FoodListFragment() {
        // Required empty public constructor
    }

    public static FoodListFragment newInstance() {
        FoodListFragment fragment = new FoodListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.getAppComponent().inject(this);
        presenter.setView(this);
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_food_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            presenter.setShouldUpdate(true);
            presenter.getData();
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.food_list_recycler_view);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        foodAdapter.setFoodList(presenter.getStoreList());
        recyclerView.setAdapter(foodAdapter);
    }

    @Override
    public void onStart() {
        presenter.getData();
        super.onStart();
    }

    @Override
    public void onSuccess(List<Food> listFood) {
        foodAdapter.setFoodList(listFood);
    }

    @Override
    public void onError(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onComplete() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showLoader() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void onStop() {
        presenter.onStop();
        super.onStop();
    }
}
