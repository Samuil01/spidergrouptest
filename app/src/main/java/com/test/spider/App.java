package com.test.spider;

import android.app.Application;

import com.test.spider.di.components.AppComponent;
import com.test.spider.di.components.DaggerAppComponent;

/**
 * Created by Admin on 17.10.2017.
 */

public class App extends Application {

    private static AppComponent appComponent;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = buildComponent();
    }

    private AppComponent buildComponent() {
        return DaggerAppComponent.builder()
                // Контекст будет нужен когда буду использовать ORM greenDao
                /*.context(this)*/
                .build();
    }
}
