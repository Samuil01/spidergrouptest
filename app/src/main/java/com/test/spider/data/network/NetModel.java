package com.test.spider.data.network;

import com.test.spider.data.model.Food;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class NetModel implements INetModel {

    ApiInterface apiServer;

    @Inject
    public NetModel(ApiInterface apiServer) {
        this.apiServer = apiServer;
    }

    @Override
    public Observable<List<Food>> getData() {
        return apiServer.getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}