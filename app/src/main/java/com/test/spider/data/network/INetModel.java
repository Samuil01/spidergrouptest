package com.test.spider.data.network;

import com.test.spider.data.model.Food;

import java.util.List;

import io.reactivex.Observable;

public interface INetModel {

    Observable<List<Food>> getData();

}