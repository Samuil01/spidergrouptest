package com.test.spider.data.network;

import com.test.spider.data.model.Food;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by Admin on 17.10.2017.
 */

public interface ApiInterface {

    @GET("test/spider/")
    Observable<List<Food>> getData();

}
