package com.test.spider.di.modules;

import android.content.Context;
import com.test.spider.di.AppScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Admin on 17.10.2017.
 */

@Module
public class AppModule {

    @AppScope
    @Provides
    public Context provideContext(Context context) {
        return context;
    }

}
