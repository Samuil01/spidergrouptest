package com.test.spider.di.components;

import com.test.spider.di.AppScope;
import com.test.spider.di.modules.FoodListModule;
import com.test.spider.di.modules.NetworkModule;
import com.test.spider.ui.foodlist.FoodListFragment;

import dagger.Component;

/**
 * Created by Admin on 17.10.2017.
 */

@Component(modules = {NetworkModule.class, FoodListModule.class}) // AppModule.class,
@AppScope
public interface AppComponent {

    void inject(FoodListFragment foodListFragment);

    @Component.Builder
    interface Builder {
        AppComponent build();
        /*@BindsInstance Builder context(Context context);*/
    }

}
