package com.test.spider.di.modules;

import com.test.spider.di.AppScope;
import com.test.spider.data.network.ApiInterface;
import com.test.spider.data.network.INetModel;
import com.test.spider.data.network.NetModel;
import com.test.spider.data.network.ServiceFactory;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Admin on 17.10.2017.
 */
@Module
public class NetworkModule {

    @AppScope
    @Provides
    public ApiInterface provideApiInterface() {
        return ServiceFactory.getService();
    }

    @AppScope
    @Provides
    INetModel provideNetModel(ApiInterface apiInterface) {
        return new NetModel(apiInterface);
    }

}
