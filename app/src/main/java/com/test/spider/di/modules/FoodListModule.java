package com.test.spider.di.modules;

import com.test.spider.di.AppScope;
import com.test.spider.data.network.NetModel;
import com.test.spider.ui.foodlist.FoodListPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Admin on 17.10.2017.
 */

@Module
public class FoodListModule {

    @AppScope
    @Provides
    public FoodListPresenter providerFoodListPresenter(NetModel netModel) {
        return new FoodListPresenter(netModel);
    }

}
